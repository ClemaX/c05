/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ten_queens_puzzle.c                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 20:32:27 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/12 21:50:25 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

int		ft_is_safe(int board[10][10], int x, int y)
{
	int i;

	i = 1;
	while (i < 10)
	{
		if ((i <= x && board[x - i][y]) ||
				(i <= x && i < 10 - y && board[x - i][y + i]) ||
				(i <= x && i <= y && board[x - i][y - i]))
			return (0);
		i++;
	}
	return (1);
}

void	ft_print_board(int board[10][10])
{
	int		x;
	int		y;
	char	c;

	x = 0;
	while (x < 10)
	{
		y = 0;
		while (y < 10)
		{
			if (board[x][y])
			{
				c = y + '0';
				write(1, &c, 1);
			}
			y++;
		}
		x++;
	}
	write(1, "\n", 1);
}

int		ft_solve(int board[10][10], int x, int *c)
{
	int y;

	y = 0;
	if (x >= 10)
		return (1);
	while (y < 10)
	{
		if (ft_is_safe(board, x, y))
		{
			board[x][y] = 1;
			if (ft_solve(board, x + 1, c))
			{
				ft_print_board(board);
				*c = *c + 1;
			}
			board[x][y] = 0;
		}
		y++;
	}
	return (0);
}

int		ft_ten_queens_puzzle(void)
{
	int board[10][10];
	int c;
	int x;
	int y;

	y = 0;
	while (y < 10)
	{
		x = 0;
		while (x < 10)
		{
			board[x][y] = 0;
			x++;
		}
		y++;
	}
	ft_solve(board, 0, &c);
	return (c);
}
