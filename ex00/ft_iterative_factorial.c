/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_iterative_factorial.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/08 14:07:33 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/08 14:24:48 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int factorial;

	if (nb < 0)
		return (0);
	if (nb <= 1)
		return (1);
	factorial = nb;
	while (nb > 1)
		factorial *= --nb;
	return (factorial);
}
